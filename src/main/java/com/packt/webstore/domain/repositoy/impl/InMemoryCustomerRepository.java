package com.packt.webstore.domain.repositoy.impl;

import com.packt.webstore.domain.Customer;
import com.packt.webstore.domain.repositoy.CustomerRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class InMemoryCustomerRepository implements CustomerRepository{

    private List<Customer> listOfCustomers = new ArrayList<>();


    public InMemoryCustomerRepository() {
        Customer jan = new Customer("C1", "Jan", "Gdynia", 2);
        Customer sylwia = new Customer("C2", "Sylwia", "Gdańsk", 5);

        listOfCustomers.add(jan);
        listOfCustomers.add(sylwia);
    }

    @Override
    public List<Customer> getAllCustomers() {
        return listOfCustomers;
    }
}
