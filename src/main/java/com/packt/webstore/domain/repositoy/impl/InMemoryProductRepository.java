package com.packt.webstore.domain.repositoy.impl;

import com.packt.webstore.domain.Customer;
import com.packt.webstore.domain.Product;
import com.packt.webstore.domain.repositoy.ProductRepository;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.*;

@Repository
public class InMemoryProductRepository implements ProductRepository{

    private List<Product> listOfProducts = new ArrayList<>();

    public InMemoryProductRepository() {
        Product iPhone = new Product("P1234", "iPhone 5s", new BigDecimal(500));
        iPhone.setDescription("Apple iPhone 5s, smartphone with 4 inch screen.");
        iPhone.setCategory("SmartPhone");
        iPhone.setManufacturer("Apple");
        iPhone.setUnitsInStock(1000);

        Product laptopDell = new Product("P1235", "Dell Inspirion", new BigDecimal(700));
        laptopDell.setDescription("Dell Inspirion, a 14 inch computer with processor Intel 4");
        laptopDell.setCategory("Laptop");
        laptopDell.setManufacturer("Dell");
        laptopDell.setUnitsInStock(1000);

        Product tabletNexus = new Product("P1236", "Nexus 7", new BigDecimal(300));
        tabletNexus.setDescription("Google Nexus 7 is the best 7 inch tablet with processor...");
        tabletNexus.setCategory("Tablet");
        tabletNexus.setManufacturer("Google");
        tabletNexus.setUnitsInStock(1000);

        listOfProducts.add(iPhone);
        listOfProducts.add(laptopDell);
        listOfProducts.add(tabletNexus);
    }

    @Override
    public List<Product> getAllProducts() {
        return listOfProducts;
    }

    @Override
    public Product getProductById(String productID) {

        Product productById = null;
        for (Product product: listOfProducts){
            if (product != null && product.getProductId() != null && product.getProductId().equals(productID)){
                productById = product;
                break;
            }
        }
        if (productById == null){
            throw new IllegalArgumentException("There is no product with this id: " + productID);
        }
        return productById;
    }

    @Override
    public List<Product> getProductsByCategory(String category) {

        List<Product> productsByCategory = new ArrayList<>();
        for (Product product: listOfProducts){
            if (category.equalsIgnoreCase(product.getCategory())) {
                productsByCategory.add(product);
            }
        }
        return productsByCategory;
    }

    @Override
    public List<Product> getProductsByManufacturer(String manufacturer) {
        List<Product> productsByMfr = new ArrayList<>();
        for (Product product: listOfProducts) {
            if (manufacturer.equalsIgnoreCase(product.getManufacturer())) {
                productsByMfr.add(product);
            }
        }
        return productsByMfr;
    }

    @Override
    public Set<Product> getProductsByFilter(Map<String, List<String>> filterParams) {

        Set<Product> productsByBrand = new HashSet<>();
        Set<Product> productsByCategory = new HashSet<>();
        Set<String> criteria = filterParams.keySet();
        if (criteria.contains("brand")) {
            for (String brandName: filterParams.get("brand")){
                for (Product product: listOfProducts) {
                    if (brandName.equalsIgnoreCase(product.getManufacturer())) {
                        productsByBrand.add(product);
                    }
                }
            }
        }
        if (criteria.contains("category")) {
            for (String category: filterParams.get("category")) {
                productsByCategory.addAll(this.getProductsByCategory(category));
            }
        }
        productsByCategory.retainAll(productsByBrand);
        return productsByCategory;
    }

    @Override
    public Set<Product> getProductByPriceFilter(Map<String, List<String>> filterParams) {
        return null;
//        Set<Product> productFromLowPrice = new HashSet<>();
//        Set<Product> productToHighPrice = new HashSet<>();
//        Set<Product> productByPrice = new HashSet<>();
//
//        Set<String> criteria = filterParams.keySet();
//        if (criteria.contains("low")) {
//            for (String lowPrice : filterParams.get("low")) {
//                for (Product product: listOfProducts) {
//                    if (product.getUnitPrice().compareTo(BigDecimal.valueOf(Double.parseDouble("low"))) == 1) {
//                        productByPrice.add(product);
//                    }
//                }
//            }
//        }
//        if (criteria.contains("high")) {
//            for (String highPrice : filterParams.get("high")) {
//                for (Product product: listOfProducts) {
//                    if (product.getUnitPrice().compareTo(BigDecimal.valueOf(Double.parseDouble("high"))) == -1) {
//                        productByPrice.add(product);
//                    }
//                }
//            }
//        }
    }

    @Override
    public void addProduct(Product product) {
        listOfProducts.add(product);
    }
}
