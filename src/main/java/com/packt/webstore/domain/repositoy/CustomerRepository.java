package com.packt.webstore.domain.repositoy;

import com.packt.webstore.domain.Customer;

import java.util.List;

public interface CustomerRepository {

    List<Customer> getAllCustomers();
}
