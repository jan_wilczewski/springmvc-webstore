package com.packt.webstore.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.InternalResourceView;

import java.util.Map;

@Controller
public class HomeController {

  @RequestMapping("/")
  public String welcome(Model model) {
    model.addAttribute("greeting", "Welcome to Web Store!");
    model.addAttribute("tagline", "The one and only amazing web store");
    return "welcome";
  }

  @RequestMapping("/welcome/greeting")
  public String greeting() {
    return "welcome";
  }






  // tak się nie powinno robić
//  @RequestMapping("/home")
//  public ModelAndView greeting(Map<String, Object> model) {
//    model.put("greeting", "Welcome in the Webstore!");
//    model.put("tagline", "The one and only not replaceable web store.");
//    View view = new InternalResourceView("/WEB-INF/views/welcome.jsp");
//    return new ModelAndView(view, model);
//  }



}
